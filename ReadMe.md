## S-Band Antenna PCBs Repository

This repository holds the pcbs for the s-band antennas: [SSA01 - Wide Bandwidth S-Band Patch Antenna](https://satsearch.co/products/exa-ssa01-wide-bandwidth-s-band-patch-antenna)

The S-Band PCB contains no components. It is a 2-layer FR-4 only PCB, with thickness of 1.0 mm, used to place the S-Band antennas. There are two antennas in the satellite. One placed in the top-x- location and the other one in the top-x+.
